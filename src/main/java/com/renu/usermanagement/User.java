/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.renu.usermanagement;

import java.io.Serializable;

/**
 *
 * @author ray
 * User Model
 */
public class User implements Serializable{
    private String userName;
    private String Password;
    
    public User(String userName, String password){
        this.userName = userName;
        this.Password = password;
    }
    
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName){
        this.userName = userName;
    }
    public String getPassword() {
        return Password;
    }
    public void setPassword(String password){
        this.Password = password;
        
    }
    @Override
    public String toString() {
        return "userName" + userName + ", password=" + Password ;
    
    }
    
}
